package zodiac

type Zodiac string

func (z Zodiac) Else(alt string) string {
	if z == "" {
		return alt
	}
	return string(z)
}

func (z Zodiac) Val() string {
	return string(z)
}
