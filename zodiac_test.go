package zodiac

import (
	"os"
	"testing"
)

func TestValue(t *testing.T) {
	os.Setenv("ZODIAC_ENV", "./test/zodiac.toml")
	os.Setenv("TEST2", "despite of my rage, i'm ...")
	os.Setenv("TEST3", "all those angels with their wings glued on")
	Load()

	type args struct {
		key string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "From Zodiac File",
			args: args{
				key: "TEST1",
			},
			want: "the world is a vampire",
		},
		{
			name: "From Env Vars",
			args: args{
				key: "TEST3",
			},
			want: "all those angels with their wings glued on",
		},
		{
			name: "In Zodiac File and Env Vars",
			args: args{
				key: "TEST2",
			},
			want: "despite of my rage, i'm ...",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Lookup(tt.args.key); got.Val() != tt.want {
				t.Errorf("Lookup() = %v, want %v", got, tt.want)
			}
		})
	}
}
