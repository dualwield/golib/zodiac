package zodiac

type zodiacEnvironment struct {
	Vars   []Var `toml:"var"`
	lookup map[string]string
}

type Var struct {
	Key   string `toml:"key"`
	Value string `toml:"val"`
}
