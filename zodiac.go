package zodiac

import (
	"github.com/pelletier/go-toml/v2"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
	"sync"
)

var definition zodiacEnvironment
var envVars map[string]string
var lock sync.Mutex

func init() {
	definition = zodiacEnvironment{
		lookup: map[string]string{},
	}
	envVars = map[string]string{}
}

func Load() error {
	lock.Lock()
	defer lock.Unlock()

	filename := os.Getenv("ZODIAC_ENV")
	var softfail bool
	if filename == "" {
		softfail = true
		filename = "zodiac.toml"
	}
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		if softfail {
			return nil
		}
		return errors.Wrapf(err, "failed to read zodiac file [%s]", filename)
	}
	if err := toml.Unmarshal(b, &definition); err != nil {
		return errors.Wrapf(err, "failed to unmarshal zodiac file [%s]", filename)
	}

	for _, v := range definition.Vars {
		definition.lookup[v.Key] = v.Value
	}
	return nil
}

// Lookup looks up in zodiac order for variable, first as env, and then as explicitly
// defined in zodiac.toml
func Lookup(key string) Zodiac {
	lock.Lock()
	v, found := envVars[key]
	if found {
		lock.Unlock()
		return Zodiac(v)
	} else {
		v = os.Getenv(key)
	}
	if v == "" {
		v = definition.lookup[key]
	} else {
		envVars[key] = v
	}
	lock.Unlock()
	return Zodiac(v)
}
