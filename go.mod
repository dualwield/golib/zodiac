module gitlab.com/dualwield/golib/zodiac

go 1.17

require (
	github.com/pelletier/go-toml/v2 v2.0.0-beta.6 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
